# Hello World

Figure out if you are referencing components correctly when trying to use them in your pipelines.

## If This Helps You, Please Star The Original Source Project
One click can help us keep providing and improving this component. If you find this information helpful, please click the star on this components original source here: [Project Details](https://gitlab.com/guided-explorations/ci-components/hello-world)

## Details

This CI Component only packages CI YAML. For an example that packages a container and YAML see the [Hello World Container Component](https://gitlab.com/explore/catalog/guided-explorations/ci-components/hello-world-container) or [Hello World Multiarch Container](https://gitlab.com/guided-explorations/ci-components/hello-world-multiarch-container)

## Usage

You should add this component to an existing `.gitlab-ci.yml` file by using the `include:`
keyword.

```yaml
include:
  - component: gitlab.com/components/hello-world@<VERSION>
```

where `<VERSION>` is the latest released tag or `main`.

### Inputs and Configuration

| Input | Default value |    Type     | Description |
| ----- | ------------- | ----------- | ----------- |
| `stage` | test | CI Component Input     | The CI Stage to run the component in. |
| `world_type` | `CI Component` | CI Component Input     | The string to insert into the middle of the Hello World log messages. |
| `HLW_CONTAINER_TAG` | `slim-v5.0.0` | CI Component Input     | Pegs container for a stable dependency, but allows override. |

### Debug Tracing

Set either CI_COMPONENT_TRACE or GitLab's global trace (CI_DEBUG_TRACE) to 'true' to get detailed debugging. More info: https://docs.gitlab.com/ee/ci/variables/#enable-debug-logging"

### Validation Test

If you simply include the component, the job log for a job called 'hello-world' should contain the logged text 'Hello CI Catalog World'

### Working Example Code Using This Component

- [Hello World Working Example Code](https://gitlab.com/guided-explorations/ci-components/working-code-examples/hello-world-component-test/)

### Related Component Projects

- [Hello World](https://gitlab.com/guided-explorations/ci-components/hello-world) - where to start if your component does not have a container.
- [Hello World Container](https://gitlab.com/guided-explorations/ci-components/hello-world-container) - where to start if your component has a single arch container.
- [Hello World Multiarch Container](https://gitlab.com/guided-explorations/ci-components/hello-world-multiarch-container) - where to start if your component needs to support multiarch containers. It was just that little more complex enough that I felt it should be distinct from the single arch container example.

## Using This Example as a Template for New Components

[How to cleanup commit history when using this as a scaffold for a new component](https://gitlab.com/guided-explorations/ci-components#using-an-existing-component-as-a-template-for-a-new-one)

## Component Building Builders Guide

[DarwinJS Component Builder Guide](https://gitlab.com/explore/catalog/guided-explorations/ci-components/gitlab-profile)

## Attributions and Credits

<a href="https://www.flaticon.com/free-icons/hello-world-container" title="hello world icons">Hello world icons created by IconBaandar - Flaticon</a>
